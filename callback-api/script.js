//this is a Promises
// const getTodos = (resource, callback) => {
//     const request = new XMLHttpRequest();

//     request.addEventListener('readystatechange', () => {
//         if(request.readyState === 4 && request.status === 200){
//             const data = JSON.parse(request.responseText);
//             callback(undefined, data);
//         }else if (request.readyState === 4) {
//             callback('could not fetch data', undefined);
//         } 
//     });

//     request.open('GET', resource); 
//     request.send(); // sending the request
// };

// //this is called nesting callback hell
// // getTodos('todos/valerie.json', (err, data) => {
// //     console.log(data);
// //     getTodos('todos/edna.json', (err, data) => {
// //         console.log(data);
// //         getTodos('todos/per.json', (err, data) => {
// //             console.log(data);
// //         })
// //     })
// // });

// // this is called promises
// const getSomething = () => {

//    return new Promise((resolve, reject) => {
//        // fetch something
//       // resolve('some data');
//        reject('some error');
//    }); 
// };

// getSomething().then( data=> {
//     console.log(data);
// }).catch(err =>{
//     console.log(err)
// }) 


// Promise Function on the go¨
const getTodos = (resource) => {

    return new Promise((resolve, reject) =>{
        const request = new XMLHttpRequest();

        request.addEventListener('readystatechange', () => {
            if(request.readyState === 4 && request.status === 200){
                const data = JSON.parse(request.responseText);
                resolve(data);
            }else if (request.readyState === 4) {
                reject('error getting resource');
            } 
        });

        request.open('GET', resource); 
        request.send(); // sending the request
    })
    
};

// getTodos('todos/edn.json').then(data =>{
//     console.log('promise is resolve:', data);
// }).catch(err => {
//     console.log('promise is rejected:', err);
// });

// Chaining Promises
getTodos('todos/edna.json').then(data =>{
    console.log('promise 1 is resolve:', data);
    return getTodos('todos/valerie.json');
}).then(data =>{
    console.log('promise 2 is resolve:', data);
    return getTodos('todos/per.json');
}).then(data => {
    console.log('promise 3 is resolve:', data);
}).catch(err => {
    console.log('promise is rejected:', err);
});