import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomeView
  },
  {
    path: '/Tickets',
    name: 'Tickets',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "tickets" */ '../views/TicketsView.vue')
  },
  {
    path: '/Films',
    name: 'Films',
    component: () => import(/* webpackChunkName: "films" */ '../views/FilmsView.vue')
  },
  {
    path: '/Store',
    name: 'Store',
    component: () => import(/* webpackChunkName: "store" */ '../views/StoreView.vue')
  },
  {
    path: '/Contact',
    name: 'Contact',
    component: () => import(/* webpackChunkName: "contact" */ '../views/ContactView.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
